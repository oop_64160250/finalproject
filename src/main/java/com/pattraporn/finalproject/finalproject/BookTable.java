/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.pattraporn.finalproject.finalproject;

/**
 *
 * @author pattiebettie
 */
public class BookTable {
    private String Name;
    private String TableNo;
    private int No;
    private String Date;
    private String Month;
    private String Hours;
    private String Minutes;
    private String Time;

    public BookTable(String Name, String TableNo, int No, String Date, String Month, String Hours, String Minutes, String Time) {
        this.Name = Name;
        this.TableNo = TableNo;
        this.No = No;
        this.Date = Date;
        this.Month = Month;
        this.Hours = Hours;
        this.Minutes = Minutes;
        this.Time = Time;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getTableNo() {
        return TableNo;
    }

    public void setTableNo(String TableNo) {
        this.TableNo = TableNo;
    }

    public int getNo() {
        return No;
    }

    public void setNo(int No) {
        this.No = No;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getMonth() {
        return Month;
    }

    public void setMonth(String Month) {
        this.Month = Month;
    }

    public String getHours() {
        return Hours;
    }

    public void setHours(String Hours) {
        this.Hours = Hours;
    }

    public String getMinutes() {
        return Minutes;
    }

    public void setMinutes(String Minutes) {
        this.Minutes = Minutes;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }

    @Override
    public String toString() {
        return "FinalProject{" + "Name=" + Name + ", TableNo=" + TableNo + ", No=" + No + ", Date=" + Date + ", Month=" + Month + ", Hours=" + Hours + ", Minutes=" + Minutes + ", Time=" + Time + '}';
    }
    
}
